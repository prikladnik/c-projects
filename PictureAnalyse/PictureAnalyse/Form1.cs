﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PictureAnalyse
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            // устанавливаем маску выбора файлов
            openFileDialog1.Filter = "Image Files(*.BMP;*.JPG;*.JPEG;*.JPE;*.GIF)|*.BMP;*.JPG;*.JPEG;*.JPE;*.GIF";
            // запускаем диалог открытия файла, и, если пользователь нажал 
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Load(openFileDialog1.FileName);
                // создаем форму
                Form2 frm2 = new Form2();
                // формируем bitmap на основе изображения в picturebox
                Bitmap img = new Bitmap(pictureBox1.Image);
                int i, j, k = 0; // k - счетчик абсолютно черных точек
                for (i = 0; i < img.Width; i++) // цикл по ширине изображения
                    for (j = 0; j < img.Height; j++) // по высоте
                    {
                        // сравниваем цвет очередного пикселя изображения с черным
                        if (img.GetPixel(i, j) == Color.FromArgb(0, 0, 0))
                            k++;
                    }
                // вызываем метод второй формы, который устанавливает значения labels с размером и кол-вом черных точек
                frm2.SetValues(
                    img.Width.ToString() + 'x' +
                    img.Height.ToString(),
                    k.ToString());
                // модально открываем вторую форму
                frm2.ShowDialog();
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            // закрываем основную форму (и само приложение)
            Close();
        }
    }
}
