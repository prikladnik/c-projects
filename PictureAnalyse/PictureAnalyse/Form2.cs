﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PictureAnalyse
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        public void SetValues(String size, String BlackCount)
        {
            labelSize.Text = size;
            labelBlackCount.Text = BlackCount;
        }
    }
}
